# ECoS EFuNN Weka module

This is the implementation of ECoS and EFuNN for Weka 3.7.

## Overview ##

This project was implemented as the final project held by Auckland Institute of Studies.

This module supports SECoS, and EFuNN algorithm.

The latest module is available at https://bitbucket.org/tonoccho/secos_efunn/downloads/SECoSSEFuNN.zip


### How to install this modules into your weka ###

* Install Weka 3.7.12
* Use package manager, install this module using url such as https://bitbucket.org/tonoccho/secos_efunn/downloads/SECoSSEFuNN.zip
* Restart weka

You will find these 2 algorithms in "misc" category.

### How to build this module ###

In this section I am explaining how to build this project. This project uses maven, so you need to install maven in advance.

To install maven, please see the url below.

* https://maven.apache.org/
* https://maven.apache.org/download.cgi

After finishing maven, use commands such as below.

* git clone https://bitbucket.org/tonoccho/secos_efunn.git
* cd secos_efunn
* mvn install

You will find the 2 jar files named such as below.

* SECoSEFuNN-x.x.x.jar
* SECoSEFuNN-x.x.x-jar-with-dependencies.jar

First one is the jar file which doesn't contain libraries that this module depends. Second one contains all of libraries that this module depends.

### Making the module by hand ###

In case you are using Mac, you can make the module by running preparepack.sh, but in case you are using Windows, as it doesn't have a command to make zip, you have to make the module by hand.

* Create a folder named "SECoSEFuNN"
* Copy files such as SECoSEFuNN-x.x.x-jar-with-dependencies.jar, entire src folder, build_package.xml, pom.xml.
* Archive the folder

To install this module, choose the file path to the zip file on package manager.
