package nz.ac.ais.weka.ecos.network.function;

/**
 * it represents the status of evolving neuron, and rule neuron. INACTIVE means, it violates
 * thresholds during ECoS algorithm.
 * 
 * @author seiji
 *
 */
public enum Status {
  ACTIVE, INACTIVE
}
