package nz.ac.ais.weka.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * this class provides methods to write execute result.
 * 
 * @author seiji
 *
 */
public class ECoSFileOutput {
  /**
   * file name to write.
   */
  private String fileName;

  /**
   * file output stream.
   */
  private FileOutputStream fos;

  /**
   * constructor, it initializes the file name.
   * 
   * @param fileName File name for output
   */
  public ECoSFileOutput(String fileName) {
    this.fileName = fileName;
  }

  /**
   * it opens the file.
   * 
   * @throws IOException Exception on IO
   */
  public void open() throws IOException {
    File file =
        new File(ECoSFileOutputDirectoryChooser.getOutputDirectory() + File.separator + fileName);
    fos = new FileOutputStream(file, true);
  }

  /**
   * it writes the string to the file.
   * 
   * @param str String to be written
   * @throws IOException Exception on IO
   */
  public void write(String str) throws IOException {
    fos.write(str.getBytes(Charset.forName("UTF-8")));
  }

  /**
   * it closes the file.
   * 
   * @throws IOException Exception on IO
   */
  public void close() throws IOException {
    fos.close();
  }

}
