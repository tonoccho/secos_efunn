package nz.ac.ais.weka.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * To avoiding the difference of OS, this class provides the method to choose the document
 * directory.
 * 
 * @author seiji
 *
 */
public class ECoSFileOutputDirectoryChooser {

  private static Logger LOG = LoggerFactory.getLogger(ECoSFileOutputDirectoryChooser.class);


  private static final String KEY_OS_NAME = "os.name";
  private static final String KEY_USER_HOME = "user.home";

  private static final String MAC_DOCUMENTS_DIR = "Documents";
  private static final String WIN_DOCUMENTS_DIR = "Documents";

  /**
   * Default constructor.
   */
  public ECoSFileOutputDirectoryChooser() {
    throw new UnsupportedOperationException();
  }

  /**
   * returns document directory.
   * 
   * @return directory name
   */
  public static String getOutputDirectory() {
    // when choosing a directory name, it is important to absorb the difference of operating system.
    // this method chooses the user's document directory.
    String osName = System.getProperty(KEY_OS_NAME);
    String userHome = System.getProperty(KEY_USER_HOME);
    String documentDir = "";
    if (osName.toUpperCase().indexOf("MAC") != -1) {
      documentDir = MAC_DOCUMENTS_DIR;
    } else if (osName.toUpperCase().indexOf("WIN") != -1) {
      documentDir = WIN_DOCUMENTS_DIR;
    } else {
      throw new UnsupportedOperationException();
    }
    String baseDir = userHome + File.separator + documentDir;

    LOG.trace("os name {} user home {} document dir {} base dir {}", osName, userHome, documentDir,
        baseDir);
    return baseDir;

  }

}
