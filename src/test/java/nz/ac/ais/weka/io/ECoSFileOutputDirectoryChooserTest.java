package nz.ac.ais.weka.io;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import java.io.File;

public class ECoSFileOutputDirectoryChooserTest {

  @Test
  public void inCaseOfMac() {
    String osNameTemp = System.getProperty("os.name");
    System.setProperty("os.name", "mac");
    assertThat(ECoSFileOutputDirectoryChooser.getOutputDirectory(),
        is(System.getProperty("user.home") + File.separator + "Documents"));
    System.setProperty("os.name", osNameTemp);
  }

  @Test
  public void inCaseOfWin() {
    String osNameTemp = System.getProperty("os.name");
    System.setProperty("os.name", "win");
    assertThat(ECoSFileOutputDirectoryChooser.getOutputDirectory(),
        is(System.getProperty("user.home") + File.separator + "Documents"));
    System.setProperty("os.name", osNameTemp);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void inCaseOfOther() {
    String osNameTemp = System.getProperty("os.name");
    try {
      System.setProperty("os.name", "other");
      assertThat(ECoSFileOutputDirectoryChooser.getOutputDirectory(),
          is(System.getProperty("user.home") + File.separator + "Documents"));
    } finally {
      System.setProperty("os.name", osNameTemp);
    }
  }

  @Test(expected = UnsupportedOperationException.class)
  public void defaultConstructorCanNotBeCalled() {
    new ECoSFileOutputDirectoryChooser();
  }

}
