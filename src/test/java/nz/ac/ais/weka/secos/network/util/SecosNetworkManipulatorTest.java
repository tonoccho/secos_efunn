package nz.ac.ais.weka.secos.network.util;

import org.junit.Test;

public class SecosNetworkManipulatorTest {

  @Test(expected = UnsupportedOperationException.class)
  public void defaultConstructorCanNotBeCalled() {
    new SecosNetworkManipulator();

  }

}
