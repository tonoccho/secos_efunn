package nz.ac.ais.weka.secos.network.util;

import org.junit.Test;

public class SecosNetworkFormatterTest {

  @Test(expected = UnsupportedOperationException.class)
  public void defaultConstructorCanNotBeCalled() {
    new SecosNetworkFormatter();
  }

}
