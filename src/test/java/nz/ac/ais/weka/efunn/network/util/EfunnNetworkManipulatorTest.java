package nz.ac.ais.weka.efunn.network.util;

import org.junit.Test;

public class EfunnNetworkManipulatorTest {

  @Test(expected = UnsupportedOperationException.class)
  public void constructorCanNotBeCalled() {
    new EfunnNetworkManipulator();
  }

}
