package nz.ac.ais.weka.efunn.network.util;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import nz.ac.ais.weka.ecos.network.function.LeftShoulderedTriangularFunction;
import nz.ac.ais.weka.ecos.network.function.RightShoulderedTriangularFunction;
import nz.ac.ais.weka.ecos.network.function.TriangularFunction;
import nz.ac.ais.weka.efunn.network.EfunnNetwork;

import org.junit.Test;

import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.File;
import java.io.IOException;

public class EfunnNetworkBuilderTest {

  @Test(expected = IllegalArgumentException.class)
  public void itThrowsExceptionWhenMappingInfoElementsForConditionNeuronNotSameAsInputNeuronCount()
      throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "13", "4,5");
  }

  @Test(expected = IllegalArgumentException.class)
  public void itThrowsExceptionWhenMappingInfoElementsForActionNeuronMustBeSameAsInputNeuronCount()
      throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "4,5", "3");
  }

  @Test(expected = IllegalArgumentException.class)
  public void itThrowsExceptionWhenMappingInfoElementsForConditionNeuronMustBe2OrMore()
      throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "1,2", "4,5");
  }

  @Test(expected = IllegalArgumentException.class)
  public void itThrowsExceptionWhenMappingInfoElementsForActionNeuronMustBe2OrMore()
      throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "4,5", "1,2");
  }

  @Test(expected = IllegalArgumentException.class)
  public void itThrowsExceptionWhenMappingInfoElementsForConditionNeuronMustBeNumeric()
      throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "A,B", "4,5");
  }

  @Test(expected = IllegalArgumentException.class)
  public void itThrowsExceptionWhenMappingInfoElementsForActionNeuronMustBeNumeric()
      throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "4,5", "A,B");
  }

  @Test
  public void itMakesInitialNetworkStructureNominal() throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetwork network =
        EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "4,5", "4,5");

    // check neuron count
    assertThat(network.getInputLayer().countNeuron(), is(2));
    assertThat(network.getConditinLayer().countNeuron(), is(9));
    assertThat(network.getRuleLayer().countNeuron(), is(0));
    assertThat(network.getActionLayer().countNeuron(), is(9));
    assertThat(network.getOutputLayer().countNeuron(), is(2));

    // check neuron name
    assertThat(network.getInputLayer().getNeuronAt(0).getName(), is("attr01"));
    assertThat(network.getInputLayer().getNeuronAt(1).getName(), is("attr02"));
    assertThat(network.getConditinLayer().getNeuronAt(0).getName(), is("Condition-0-0"));
    assertThat(network.getConditinLayer().getNeuronAt(1).getName(), is("Condition-0-1"));
    assertThat(network.getConditinLayer().getNeuronAt(2).getName(), is("Condition-0-2"));
    assertThat(network.getConditinLayer().getNeuronAt(3).getName(), is("Condition-0-3"));
    assertThat(network.getConditinLayer().getNeuronAt(4).getName(), is("Condition-1-0"));
    assertThat(network.getConditinLayer().getNeuronAt(5).getName(), is("Condition-1-1"));
    assertThat(network.getConditinLayer().getNeuronAt(6).getName(), is("Condition-1-2"));
    assertThat(network.getConditinLayer().getNeuronAt(7).getName(), is("Condition-1-3"));
    assertThat(network.getConditinLayer().getNeuronAt(8).getName(), is("Condition-1-4"));
    assertThat(network.getActionLayer().getNeuronAt(0).getName(), is("Action-0-0"));
    assertThat(network.getActionLayer().getNeuronAt(1).getName(), is("Action-0-1"));
    assertThat(network.getActionLayer().getNeuronAt(2).getName(), is("Action-0-2"));
    assertThat(network.getActionLayer().getNeuronAt(3).getName(), is("Action-0-3"));
    assertThat(network.getActionLayer().getNeuronAt(4).getName(), is("Action-1-0"));
    assertThat(network.getActionLayer().getNeuronAt(5).getName(), is("Action-1-1"));
    assertThat(network.getActionLayer().getNeuronAt(6).getName(), is("Action-1-2"));
    assertThat(network.getActionLayer().getNeuronAt(7).getName(), is("Action-1-3"));
    assertThat(network.getActionLayer().getNeuronAt(8).getName(), is("Action-1-4"));
    assertThat(network.getOutputLayer().getNeuronAt(0).getName(), is("nom01"));
    assertThat(network.getOutputLayer().getNeuronAt(1).getName(), is("nom02"));

    // check mf type
    assertThat(network.getConditinLayer().getNeuronAt(0).getMf(),
        is(instanceOf(LeftShoulderedTriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(1).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(2).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(3).getMf(),
        is(instanceOf(RightShoulderedTriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(4).getMf(),
        is(instanceOf(LeftShoulderedTriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(5).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(6).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(7).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(8).getMf(),
        is(instanceOf(RightShoulderedTriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(0).getMf(),
        is(instanceOf(LeftShoulderedTriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(1).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(2).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(3).getMf(),
        is(instanceOf(RightShoulderedTriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(4).getMf(),
        is(instanceOf(LeftShoulderedTriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(5).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(6).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(7).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(8).getMf(),
        is(instanceOf(RightShoulderedTriangularFunction.class)));

    // check MF center value
    assertThat(network.getConditinLayer().getNeuronAt(0).getMf().getCenter(), is(0.0));
    assertThat(network.getConditinLayer().getNeuronAt(1).getMf().getCenter(),
        is(0.3333333333333333));
    assertThat(network.getConditinLayer().getNeuronAt(2).getMf().getCenter(),
        is(0.6666666666666666));
    assertThat(network.getConditinLayer().getNeuronAt(3).getMf().getCenter(), is(1.0));
    assertThat(network.getConditinLayer().getNeuronAt(4).getMf().getCenter(), is(0.0));
    assertThat(network.getConditinLayer().getNeuronAt(5).getMf().getCenter(), is(0.25));
    assertThat(network.getConditinLayer().getNeuronAt(6).getMf().getCenter(), is(0.5));
    assertThat(network.getConditinLayer().getNeuronAt(7).getMf().getCenter(), is(0.75));
    assertThat(network.getConditinLayer().getNeuronAt(8).getMf().getCenter(), is(1.0));
    assertThat(network.getActionLayer().getNeuronAt(0).getMf().getCenter(), is(0.0));
    assertThat(network.getActionLayer().getNeuronAt(1).getMf().getCenter(), is(0.3333333333333333));
    assertThat(network.getActionLayer().getNeuronAt(2).getMf().getCenter(), is(0.6666666666666666));
    assertThat(network.getActionLayer().getNeuronAt(3).getMf().getCenter(), is(1.0));
    assertThat(network.getActionLayer().getNeuronAt(4).getMf().getCenter(), is(0.0));
    assertThat(network.getActionLayer().getNeuronAt(5).getMf().getCenter(), is(0.25));
    assertThat(network.getActionLayer().getNeuronAt(6).getMf().getCenter(), is(0.5));
    assertThat(network.getActionLayer().getNeuronAt(7).getMf().getCenter(), is(0.75));
    assertThat(network.getActionLayer().getNeuronAt(8).getMf().getCenter(), is(1.0));

    // check mapping index
    assertThat(network.getConditinLayer().getNeuronAt(0).getMappingIndex(), is(0));
    assertThat(network.getConditinLayer().getNeuronAt(1).getMappingIndex(), is(0));
    assertThat(network.getConditinLayer().getNeuronAt(2).getMappingIndex(), is(0));
    assertThat(network.getConditinLayer().getNeuronAt(3).getMappingIndex(), is(0));
    assertThat(network.getConditinLayer().getNeuronAt(4).getMappingIndex(), is(1));
    assertThat(network.getConditinLayer().getNeuronAt(5).getMappingIndex(), is(1));
    assertThat(network.getConditinLayer().getNeuronAt(6).getMappingIndex(), is(1));
    assertThat(network.getConditinLayer().getNeuronAt(7).getMappingIndex(), is(1));
    assertThat(network.getConditinLayer().getNeuronAt(8).getMappingIndex(), is(1));
    assertThat(network.getActionLayer().getNeuronAt(0).getMappingIndex(), is(0));
    assertThat(network.getActionLayer().getNeuronAt(1).getMappingIndex(), is(0));
    assertThat(network.getActionLayer().getNeuronAt(2).getMappingIndex(), is(0));
    assertThat(network.getActionLayer().getNeuronAt(3).getMappingIndex(), is(0));
    assertThat(network.getActionLayer().getNeuronAt(4).getMappingIndex(), is(1));
    assertThat(network.getActionLayer().getNeuronAt(5).getMappingIndex(), is(1));
    assertThat(network.getActionLayer().getNeuronAt(6).getMappingIndex(), is(1));
    assertThat(network.getActionLayer().getNeuronAt(7).getMappingIndex(), is(1));
    assertThat(network.getActionLayer().getNeuronAt(8).getMappingIndex(), is(1));

  }

  @Test
  public void itMakesInitialNetworkStructureNumeric() throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNumeric.arff";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetwork network =
        EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "3,3", "3");

    // check neuron count
    assertThat(network.getInputLayer().countNeuron(), is(2));
    assertThat(network.getConditinLayer().countNeuron(), is(6));
    assertThat(network.getRuleLayer().countNeuron(), is(0));
    assertThat(network.getActionLayer().countNeuron(), is(3));
    assertThat(network.getOutputLayer().countNeuron(), is(1));

    // check neuron name
    assertThat(network.getInputLayer().getNeuronAt(0).getName(), is("attr01"));
    assertThat(network.getInputLayer().getNeuronAt(1).getName(), is("attr02"));
    assertThat(network.getConditinLayer().getNeuronAt(0).getName(), is("Condition-0-0"));
    assertThat(network.getConditinLayer().getNeuronAt(1).getName(), is("Condition-0-1"));
    assertThat(network.getConditinLayer().getNeuronAt(2).getName(), is("Condition-0-2"));
    assertThat(network.getConditinLayer().getNeuronAt(3).getName(), is("Condition-1-0"));
    assertThat(network.getConditinLayer().getNeuronAt(4).getName(), is("Condition-1-1"));
    assertThat(network.getConditinLayer().getNeuronAt(5).getName(), is("Condition-1-2"));
    assertThat(network.getActionLayer().getNeuronAt(0).getName(), is("Action-0-0"));
    assertThat(network.getActionLayer().getNeuronAt(1).getName(), is("Action-0-1"));
    assertThat(network.getActionLayer().getNeuronAt(2).getName(), is("Action-0-2"));
    assertThat(network.getOutputLayer().getNeuronAt(0).getName(), is("attr03"));

    // check mf type
    assertThat(network.getConditinLayer().getNeuronAt(0).getMf(),
        is(instanceOf(LeftShoulderedTriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(1).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(2).getMf(),
        is(instanceOf(RightShoulderedTriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(3).getMf(),
        is(instanceOf(LeftShoulderedTriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(4).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getConditinLayer().getNeuronAt(5).getMf(),
        is(instanceOf(RightShoulderedTriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(0).getMf(),
        is(instanceOf(LeftShoulderedTriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(1).getMf(),
        is(instanceOf(TriangularFunction.class)));
    assertThat(network.getActionLayer().getNeuronAt(2).getMf(),
        is(instanceOf(RightShoulderedTriangularFunction.class)));

    // check MF center value
    assertThat(network.getConditinLayer().getNeuronAt(0).getMf().getCenter(), is(0.0));
    assertThat(network.getConditinLayer().getNeuronAt(1).getMf().getCenter(), is(0.5));
    assertThat(network.getConditinLayer().getNeuronAt(2).getMf().getCenter(), is(1.0));
    assertThat(network.getConditinLayer().getNeuronAt(3).getMf().getCenter(), is(0.0));
    assertThat(network.getConditinLayer().getNeuronAt(4).getMf().getCenter(), is(0.5));
    assertThat(network.getConditinLayer().getNeuronAt(5).getMf().getCenter(), is(1.0));
    assertThat(network.getActionLayer().getNeuronAt(0).getMf().getCenter(), is(0.0));
    assertThat(network.getActionLayer().getNeuronAt(1).getMf().getCenter(), is(0.5));
    assertThat(network.getActionLayer().getNeuronAt(2).getMf().getCenter(), is(1.0));

    // check mapping index
    assertThat(network.getConditinLayer().getNeuronAt(0).getMappingIndex(), is(0));
    assertThat(network.getConditinLayer().getNeuronAt(1).getMappingIndex(), is(0));
    assertThat(network.getConditinLayer().getNeuronAt(2).getMappingIndex(), is(0));
    assertThat(network.getConditinLayer().getNeuronAt(3).getMappingIndex(), is(1));
    assertThat(network.getConditinLayer().getNeuronAt(4).getMappingIndex(), is(1));
    assertThat(network.getConditinLayer().getNeuronAt(5).getMappingIndex(), is(1));
    assertThat(network.getActionLayer().getNeuronAt(0).getMappingIndex(), is(0));
    assertThat(network.getActionLayer().getNeuronAt(1).getMappingIndex(), is(0));
    assertThat(network.getActionLayer().getNeuronAt(2).getMappingIndex(), is(0));

  }

  @Test(expected = UnsupportedOperationException.class)
  public void itThrowsExceptionWhenTryToInstantiate() {
    new EfunnNetworkBuilder();
  }
}
