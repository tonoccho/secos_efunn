package nz.ac.ais.weka.efunn.network.util;


import org.junit.Test;

public class EfunnNetworkTrainerTest {

  @Test(expected = UnsupportedOperationException.class)
  public void defaultConstructorCanNtBeCalled() {
    new EfunnNetworkTrainer();
  }

}
