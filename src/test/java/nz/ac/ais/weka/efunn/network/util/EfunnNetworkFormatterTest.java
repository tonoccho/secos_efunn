package nz.ac.ais.weka.efunn.network.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import nz.ac.ais.weka.efunn.network.EfunnNetwork;
import org.junit.Test;
import weka.classifiers.misc.EfunnClassifier;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;

public class EfunnNetworkFormatterTest {
  @Test(expected = UnsupportedOperationException.class)
  public void constructorCanNotBeCalled() {
    new EfunnNetworkFormatter();
  }

  @Test
  public void itMakesStringRepresentativeOfInitialNetwork() throws IOException {
    final String datasetFile =
        "src/test/resources/EfunnNetworkBuilderTest-itMakesInitialNetworkStructureNominal.arff";
    final String expextedOutputFile =
        "src/test/resources/EfunnNetworkFormatterTest-itMakesStringRepresentativeOfNetwork-"
            + "ExpectedOutput.txt";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File(datasetFile));
    Instances instances = loader.getDataSet();

    EfunnNetwork network =
        EfunnNetworkBuilder.buildInitialNetwork(instances.firstInstance(), "4,5", "4,5");

    FileInputStream fis = new FileInputStream(new File(expextedOutputFile));
    byte[] buf = new byte[fis.available()];
    fis.read(buf);
    fis.close();
    String expectedOutputString = new String(buf, Charset.forName("UTF-8"));
    assertThat(EfunnNetworkFormatter.format(network), is(expectedOutputString));
  }

  @Test
  public void itMakesStringRepresentativeOfMaturedNetwork() throws Exception {
    final String expextedOutputFile =
        "src/test/resources/EfunnNetworkFormatterTest-itMakesStringRepresentativeOfMaturedNetwork-"
            + "ExpectedOutput.txt";

    ArffLoader loader = new ArffLoader();
    loader.setFile(new File("src/test/resources/iris_ecos.arff"));
    Instances instances = loader.getDataSet();

    EfunnClassifier classifier = new EfunnClassifier();
    classifier.setErrorThreshold(0.01);
    classifier.setSensitivityThreshold(0.5);
    classifier.setLearningRate1(0.5);
    classifier.setLearningRate2(0.5);
    classifier.setInputMfMapping("3,3,3,3");
    classifier.setOutputMfMapping("2,2,2");
    classifier.buildClassifier(instances);

    FileInputStream fis = new FileInputStream(new File(expextedOutputFile));
    byte[] buf = new byte[fis.available()];
    fis.read(buf);
    fis.close();
    String expectedOutputString = new String(buf, Charset.forName("UTF-8"));

    System.out.println(classifier.toString());

    // Classifier#toString calls formatter directly
    assertThat(classifier.toString(), is(expectedOutputString));
  }
}
