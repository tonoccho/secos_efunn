package nz.ac.ais.weka.ecos.network.function;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class TriangularFunctionTest {
  private TriangularFunction function = null;

  @Before
  public void before() {
    function = new TriangularFunction();
  }

  @After
  public void after() {
    function = null;
  }

  @Test
  public void itReturns1IfInputVectorEqualsToCenter() {
    function.setLeft(0.0);
    function.setCenter(0.5);
    function.setRight(1.0);
    assertThat(function.fuzzify(0.5), is(1.0));
  }

  @Test
  public void itReturnsFuzzifiedValue() {
    function.setLeft(0.0);
    function.setCenter(0.5);
    function.setRight(1.0);
    assertThat(new BigDecimal(function.fuzzify(0.1)).setScale(1, BigDecimal.ROUND_HALF_UP)
        .doubleValue(), is(0.2));
    assertThat(new BigDecimal(function.fuzzify(0.4)).setScale(1, BigDecimal.ROUND_HALF_UP)
        .doubleValue(), is(0.8));
    assertThat(new BigDecimal(function.fuzzify(0.6)).setScale(1, BigDecimal.ROUND_HALF_UP)
        .doubleValue(), is(0.8));
    assertThat(new BigDecimal(function.fuzzify(0.9)).setScale(1, BigDecimal.ROUND_HALF_UP)
        .doubleValue(), is(0.2));
  }

  @Test
  public void itReturns0IfValueIsOutOfRange() {
    function.setLeft(0.0);
    function.setCenter(0.5);
    function.setRight(1.0);
    assertThat(function.fuzzify(-0.1), is(0.0));
    assertThat(function.fuzzify(0.0), is(0.0));
    assertThat(function.fuzzify(1.0), is(0.0));
    assertThat(function.fuzzify(1.1), is(0.0));

  }
}
