package nz.ac.ais.weka.ecos.network.function;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import nz.ac.ais.weka.ecos.network.ECoSNetwork;
import nz.ac.ais.weka.efunn.network.EfunnNetwork;
import nz.ac.ais.weka.efunn.network.layer.EfunnActionLayer;
import nz.ac.ais.weka.efunn.network.layer.EfunnRuleLayer;
import nz.ac.ais.weka.efunn.network.neuron.EfunnActionNeuron;
import nz.ac.ais.weka.efunn.network.neuron.EfunnRuleNeuron;
import nz.ac.ais.weka.secos.network.SecosNetwork;
import nz.ac.ais.weka.secos.network.layer.SecosEvolvingLayer;
import nz.ac.ais.weka.secos.network.layer.SecosOutputLayer;
import nz.ac.ais.weka.secos.network.neuron.SecosEvolvingNeuron;
import nz.ac.ais.weka.secos.network.neuron.SecosOutputNeuron;

import org.junit.Test;

/**
 * This class tests EcosAlgorithe.
 *
 * @author seiji
 *
 */
public class EcosAlgorithmTest extends ECoSAlgorithm {

  /**
   * When EvolvingLayer ( in terms of SECoS ), or RuleLayer ( in terms of EFuNN ) is empty, it
   * ECoSAlgorithm returns ALL_INACTIVE. The Algorithm ignores the state of the network, and
   * parameters.
   */
  @Test
  public final void itReturnsAllInactiveIfEvolvingLayerIsEmptyForSecos() {

    SecosNetwork network = new SecosNetwork();
    network.setEvolvingLayer(new SecosEvolvingLayer("name"));
    assertThat(ECoSAlgorithm.execute(network, 0.0, 0.0, 0.0, 0.0), is(ECoSAnswer.ALL_INACTIVE));

  }

  /**
   * When EvolvingLayer ( in terms of SECoS ), or RuleLayer ( in terms of EFuNN ) is empty, it
   * ECoSAlgorithm returns ALL_INACTIVE. The Algorithm ignores the state of the network, and
   * parameters.
   */
  @Test
  public final void itReturnsAllInactiveIfEvolvingLayerIsEmptyForEfunn() {

    EfunnNetwork network = new EfunnNetwork();
    network.setRuleLayer(new EfunnRuleLayer("name"));
    assertThat(ECoSAlgorithm.execute(network, 0.0, 0.0, 0.0, 0.0), is(ECoSAnswer.ALL_INACTIVE));

  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowNullForNetwork() {
    ECoSAlgorithm.execute(null, 0, 0, 0, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMinusForSensitivityThreshold() {
    ECoSAlgorithm.execute(new SecosNetwork(), -0.1, 0, 0, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMoreThan1ForSensitivityThreshold() {
    ECoSAlgorithm.execute(new SecosNetwork(), 1.1, 0, 0, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMinusForErrorThreshold() {
    ECoSAlgorithm.execute(new SecosNetwork(), 0, -0.1, 0, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMoreThan1ForErrorThreshold() {
    ECoSAlgorithm.execute(new SecosNetwork(), 0, 1.1, 0, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMinusForLearningRate1() {
    ECoSAlgorithm.execute(new SecosNetwork(), 0, 0, -0.1, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMoreThan1ForLearningRate1() {
    ECoSAlgorithm.execute(new SecosNetwork(), 0, 0, 1.1, 0);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMinusForLearningRate2() {
    ECoSAlgorithm.execute(new SecosNetwork(), 0, 0, 0, -0.1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void itDoesNotAllowMoreThan1ForLearningRate2() {
    ECoSAlgorithm.execute(new SecosNetwork(), 0, 0, 0, 1.1);
  }

  @Test
  public void itUpdatesActivationStatus() {
    {
      SecosNetwork network = new SecosNetwork();
      network.setEvolvingLayer(new SecosEvolvingLayer("name"));
      SecosEvolvingNeuron neuron1 = new SecosEvolvingNeuron("name");
      neuron1.setActivation(0.5);
      network.getEvolvingLayer().addNeuron(neuron1);

      // When activation is smaller than sesitivity threshold, neuron will
      // be inactive
      assertThat(ECoSAlgorithm.execute(network, 0.9, 0.0, 0.0, 0.0), is(ECoSAnswer.ALL_INACTIVE));
      assertThat(network.getEvolvingLayer().getNeuronAt(0).getStatus(), is(Status.INACTIVE));

    }

    {
      SecosNetwork network = new SecosNetwork();
      network.setEvolvingLayer(new SecosEvolvingLayer("name"));
      SecosEvolvingNeuron neuron1 = new SecosEvolvingNeuron("name");
      neuron1.setActivation(0.5);
      network.getEvolvingLayer().addNeuron(neuron1);

      // if one neuron is active, returns winner_exists
      assertThat(ECoSAlgorithm.execute(network, 0.1, 0.0, 0.0, 0.0), is(ECoSAnswer.WINNER_EXISTS));
      assertThat(network.getEvolvingLayer().getNeuronAt(0).getStatus(), is(Status.ACTIVE));

    }

    {
      SecosNetwork network = new SecosNetwork();
      network.setEvolvingLayer(new SecosEvolvingLayer("name"));
      SecosEvolvingNeuron neuron1 = new SecosEvolvingNeuron("name");
      neuron1.setActivation(0.5);
      network.getEvolvingLayer().addNeuron(neuron1);

      // if one neuron is active, returns winner_exists
      assertThat(ECoSAlgorithm.execute(network, 0.1, 0.0, 0.0, 0.0), is(ECoSAnswer.WINNER_EXISTS));
      assertThat(network.getEvolvingLayer().getNeuronAt(0).getStatus(), is(Status.ACTIVE));

    }
  }

  @Test
  public void itChecksErrorthreshold() {
    {
      SecosNetwork network = new SecosNetwork();
      network.setEvolvingLayer(new SecosEvolvingLayer("evo"));
      network.setOutputLayer(new SecosOutputLayer("out"));
      SecosEvolvingNeuron evolvingNeuron = new SecosEvolvingNeuron("evon");
      SecosOutputNeuron outputNeuron = new SecosOutputNeuron("outn");
      evolvingNeuron.setActivation(0.1);
      outputNeuron.setDesiredOutput(0.5);
      IncomingWeightVector<SecosEvolvingNeuron, SecosOutputNeuron> incomingVector =
          new IncomingWeightVector<SecosEvolvingNeuron, SecosOutputNeuron>("vec");
      incomingVector.setWeight(0.3);
      evolvingNeuron.addRightConnection(incomingVector);
      outputNeuron.addLeftConnection(incomingVector);
      incomingVector.setLeftNeuron(evolvingNeuron);
      incomingVector.setRightNeuron(outputNeuron);

      network.getEvolvingLayer().addNeuron(evolvingNeuron);
      network.getOutputLayer().addNeuron(outputNeuron);

      assertThat(ECoSAlgorithm.execute(network, 0.0, 0.3, 0.0, 0.0), is(ECoSAnswer.ALL_INACTIVE));
      assertThat(network.getEvolvingLayer().getNeuronAt(0).getStatus(), is(Status.INACTIVE));

    }

    {
      SecosNetwork network = new SecosNetwork();
      network.setEvolvingLayer(new SecosEvolvingLayer("evo"));
      network.setOutputLayer(new SecosOutputLayer("out"));
      SecosEvolvingNeuron evolvingNeuron = new SecosEvolvingNeuron("evon");
      SecosOutputNeuron outputNeuron = new SecosOutputNeuron("outn");
      evolvingNeuron.setActivation(0.1);
      outputNeuron.setDesiredOutput(0.5);
      IncomingWeightVector<SecosEvolvingNeuron, SecosOutputNeuron> incomingVector =
          new IncomingWeightVector<SecosEvolvingNeuron, SecosOutputNeuron>("vec");
      incomingVector.setWeight(0.3);
      evolvingNeuron.addRightConnection(incomingVector);
      outputNeuron.addLeftConnection(incomingVector);
      incomingVector.setLeftNeuron(evolvingNeuron);
      incomingVector.setRightNeuron(outputNeuron);

      network.getEvolvingLayer().addNeuron(evolvingNeuron);
      network.getOutputLayer().addNeuron(outputNeuron);

      assertThat(ECoSAlgorithm.execute(network, 0.0, 1.0, 0.0, 0.0), is(ECoSAnswer.WINNER_EXISTS));
      assertThat(network.getEvolvingLayer().getNeuronAt(0).getStatus(), is(Status.ACTIVE));

    }

    {
      EfunnNetwork network = new EfunnNetwork();
      network.setRuleLayer(new EfunnRuleLayer("rule"));
      network.setActionLayer(new EfunnActionLayer("action"));
      EfunnRuleNeuron evolvingNeuron = new EfunnRuleNeuron("rulen");
      EfunnActionNeuron outputNeuron = new EfunnActionNeuron("actn");
      evolvingNeuron.setActivation(0.1);
      outputNeuron.setActivation(0.5);
      IncomingWeightVector<EfunnRuleNeuron, EfunnActionNeuron> incomingVector =
          new IncomingWeightVector<EfunnRuleNeuron, EfunnActionNeuron>("vec");
      incomingVector.setWeight(0.3);
      evolvingNeuron.addRightConnection(incomingVector);
      outputNeuron.addLeftConnection(incomingVector);
      incomingVector.setLeftNeuron(evolvingNeuron);
      incomingVector.setRightNeuron(outputNeuron);

      network.getRuleLayer().addNeuron(evolvingNeuron);
      network.getActionLayer().addNeuron(outputNeuron);

      assertThat(ECoSAlgorithm.execute(network, 0.0, 0.3, 0.0, 0.0), is(ECoSAnswer.ALL_INACTIVE));
      assertThat(network.getRuleLayer().getNeuronAt(0).getStatus(), is(Status.INACTIVE));

    }

    {
      EfunnNetwork network = new EfunnNetwork();
      network.setRuleLayer(new EfunnRuleLayer("rule"));
      network.setActionLayer(new EfunnActionLayer("action"));
      EfunnRuleNeuron evolvingNeuron = new EfunnRuleNeuron("rulen");
      EfunnActionNeuron outputNeuron = new EfunnActionNeuron("actn");
      evolvingNeuron.setActivation(0.1);
      outputNeuron.setActivation(0.5);
      IncomingWeightVector<EfunnRuleNeuron, EfunnActionNeuron> incomingVector =
          new IncomingWeightVector<EfunnRuleNeuron, EfunnActionNeuron>("vec");
      incomingVector.setWeight(0.3);
      evolvingNeuron.addRightConnection(incomingVector);
      outputNeuron.addLeftConnection(incomingVector);
      incomingVector.setLeftNeuron(evolvingNeuron);
      incomingVector.setRightNeuron(outputNeuron);

      network.getRuleLayer().addNeuron(evolvingNeuron);
      network.getActionLayer().addNeuron(outputNeuron);

      assertThat(ECoSAlgorithm.execute(network, 0.0, 1.0, 0.0, 0.0), is(ECoSAnswer.WINNER_EXISTS));
      assertThat(network.getRuleLayer().getNeuronAt(0).getStatus(), is(Status.ACTIVE));

    }
  }

  @Test(expected = UnsupportedOperationException.class)
  public void itAllowsOnlySecosAndEfunnNetwork() {
    ECoSAlgorithm.execute(new ECoSNetwork() {}, 0, 0, 0, 0);
  }
}
