package nz.ac.ais.weka.ecos.network.function;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class RightShoulderedTriangularFunctionTest {

  private RightShoulderedTriangularFunction function = null;

  @Before
  public void before() {
    function = new RightShoulderedTriangularFunction();
  }

  @After
  public void after() {
    function = null;
  }

  @Test
  public void itReturns1IfInputVectorEqualsToCenter() {
    function.setLeft(0.0);
    function.setCenter(1.0);
    assertThat(function.fuzzify(1.0), is(1.0));
  }

  @Test
  public void itReturnsFuzzifiedValue() {
    function.setLeft(0.0);
    function.setCenter(1.0);
    assertThat(new BigDecimal(function.fuzzify(0.1)).setScale(1, BigDecimal.ROUND_HALF_UP)
        .doubleValue(), is(0.1));
    assertThat(function.fuzzify(0.9), is(0.9));
  }

  @Test
  public void itReturns0IfValueIsOutOfRange() {
    function.setLeft(0.0);
    function.setCenter(1.0);
    assertThat(function.fuzzify(-0.1), is(0.0));
    assertThat(function.fuzzify(0.0), is(0.0));
    assertThat(function.fuzzify(1.1), is(0.0));

  }
}
