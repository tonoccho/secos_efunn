package nz.ac.ais.weka.ecos.network.neuron;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class NullNeuronTest {

  @Test
  public void itHasName() {
    NullNeuron neuron = new NullNeuron("Test Name");
    assertThat(neuron.getName(), is("Test Name"));
  }

}
