package nz.ac.ais.weka.ecos.network.function;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import nz.ac.ais.weka.secos.network.neuron.SecosEvolvingNeuron;
import nz.ac.ais.weka.secos.network.neuron.SecosInputNeuron;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IncomingWeightVectorTest {
  private IncomingWeightVector<SecosInputNeuron, SecosEvolvingNeuron> incomingWeightVector = null;

  @Before
  public void before() {
    incomingWeightVector = new IncomingWeightVector<SecosInputNeuron, SecosEvolvingNeuron>("test");
  }

  @After
  public void after() {
    incomingWeightVector = null;
  }

  @Test
  public void toStringReturnsDetailedInfo() {
    incomingWeightVector.setLeftNeuron(new SecosInputNeuron("input"));
    incomingWeightVector.setRightNeuron(new SecosEvolvingNeuron("evolving"));
    String expected = "[LeftNeuron-input]-[ItsName-test]-[RightNeuron-evolving]-[Weight0.0]";
    assertThat(incomingWeightVector.toString(), is(expected));
  }
  
  @Test
  public void toSimpleStringReturnsSimplifiedInfo() {
    incomingWeightVector.setLeftNeuron(new SecosInputNeuron("input"));
    incomingWeightVector.setRightNeuron(new SecosEvolvingNeuron("evolving"));
    String expected = "test-0.0";
    assertThat(incomingWeightVector.toSimpleString(), is(expected));
  }

}
