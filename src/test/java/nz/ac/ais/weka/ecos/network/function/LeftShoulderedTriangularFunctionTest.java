package nz.ac.ais.weka.ecos.network.function;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class LeftShoulderedTriangularFunctionTest {
  private LeftShoulderedTriangularFunction function = null;

  @Before
  public void before() {
    function = new LeftShoulderedTriangularFunction();
  }

  @After
  public void after() {
    function = null;
  }

  @Test
  public void itReturns1IfInputVectorEqualsToCenter() {
    function.setCenter(0.5);
    assertThat(function.fuzzify(0.5), is(1.0));
  }

  @Test
  public void itReturnsFuzzifiedValue() {
    function.setCenter(0.0);
    function.setRight(1.0);
    assertThat(function.fuzzify(0.1), is(0.9));
    assertThat(function.fuzzify(0.5), is(0.5));
    assertThat(new BigDecimal(function.fuzzify(0.9)).setScale(1, BigDecimal.ROUND_HALF_UP)
        .doubleValue(), is(0.1));
  }

  @Test
  public void itReturns0IfInputVectorIsOutOfRange() {
    function.setCenter(0.0);
    function.setRight(1.0);

    assertThat(function.fuzzify(-0.1), is(0.0));
    assertThat(function.fuzzify(1.0), is(0.0));
    assertThat(function.fuzzify(1.1), is(0.0));
  }

}
